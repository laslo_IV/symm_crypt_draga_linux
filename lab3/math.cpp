#include "header_math.h"


int inverse_element(int a, int n)
{
    while ( a < 0)
    {
        a += n;
    }
    if (a == 0)
    {
        return -1;
    }

    if (a == 1)
    {
        return a;
    }

    if (!(n % a))
    {
        return -1;
    }

    if (__gcd(a, n) > 1)
    {
        return -1;
    }

    int s_n = n; // зберігаємо n, щоб вивести відповідь у діапазоні 0..n-1
    int r_n  = 0;
    int v_0 = 0, v_1 = 1; 
    int tmp_v = 0;
    do
    {
        r_n = n % a;
    
        tmp_v = v_0 - v_1 * ((n - r_n) / a);
        v_0 = v_1;
        v_1 = tmp_v;

        n = a;
        a = r_n;

    }while(r_n != 1);

    while(v_1 < 0)
    {
        v_1 += s_n;
    }
    return v_1;
}



void get_key(const int* X, const int* Y, multimap<int, int>& K)
{
    int a = 0, b = 0, n = 0;
    int x = X[0] - X[1];
    int y = Y[0] - Y[1];
    
    while (x < 0)
    {
        x += BIGRAM_SIZE;
    }
    while(y < 0)
    {
        y += BIGRAM_SIZE;
    }

    int d = __gcd(y, BIGRAM_SIZE);

    if (!(y % d) && d > 1)
    {
        return;
    }

    n = BIGRAM_SIZE / d;
    x = x / d;
    y = y / d;

    x = inverse_element(x, n);

    a = (x * y) % BIGRAM_SIZE ;

    for (int i = 0; i < d; i++)
    {
        b = ((Y[0] - a * X[0]) + a*BIGRAM_SIZE) % BIGRAM_SIZE;
        K.insert(pair<int, int> (a + n*i, b));
    }
}











// int _gcd(int a, int b)
// {
//     int r_n  = 1;
//     while (r_n)
//     {
//         r_n = a % b;
//         a = b;
//         b = r_n;
//     }
//     return a;
// }