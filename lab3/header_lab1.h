#pragma once
#include <string>
#include <fstream>
#include <cstdio>
#include <map>
#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;
const int TABLE_SIZE = 33;
const int ALPHABET_SIZE = 31;
const int BIGRAM_SIZE = ALPHABET_SIZE * ALPHABET_SIZE;

//видалення зайвих символі
void TextFormatting(string pathToText);

void StringFormatting(wstring& text);

//зчитування файлу в змінну (по замовчуваню з пробілами)
void ReadFile(string ipath, wstring& text, bool with_space = 0);

//знайти частоти букв в тексті
void FrequencyChar(wstring& text, map<wchar_t, double>& alf);

//знайти чатоти біграм в файлі (з перетинами)
void FrequencyBigram_v1(wstring& text, string opath);

//знайти чатоти біграм в файлі (без перетинів)
void FrequencyBigram_v2(wstring& text, string opath);
 
//обрахувати ентропію
double H(string ipath);


double H(map<wstring, double>& bigrams );
double H(map<wchar_t, double>& bigrams );

//записати частоти n-грам в файл opath (по замовчуваню з перетинами)
//void FrequencyNgram(unsigned int N, string& text, string opath, bool with_intersection = 1);
//int Symbol(unsigned char);
//double H_1(string);
//double H_2(string);
//void FrequencyChar_f(string, string);
//void FrequencyBigram_v1_f(string, string);
//void FrequencyBigram_v2_f(string, string);












