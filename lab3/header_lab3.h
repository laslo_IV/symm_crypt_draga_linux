#include <iostream>
#include <fstream>
#include "header_lab1.h"
#include <sys/types.h>
#include <dirent.h>

using namespace std;

void FrequencyBigram(wstring& text, string opath);
void encrypt(wstring& d_text, int* k, string opath);
int decrypt(wstring& c_text, int* k, string opath);
void FrequencyBigram(wstring& text, map<wstring, double>& fr);
void foo(string file_t, string file_c, string name);
void weed_out(string path);