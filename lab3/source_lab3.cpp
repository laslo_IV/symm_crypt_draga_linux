#include "header_lab3.h"
#include "header_math.h"
#include <vector>

void FrequencyBigram(wstring& text, string opath)
{
    int** table = new(nothrow) int* [TABLE_SIZE];

    for (int i = 0; i < TABLE_SIZE; i++)
    {
        table[i] = new(nothrow) int[TABLE_SIZE] {};
    }

    if (text.length() % 2) 
    {
        text = text.substr(0, text.length() - 1);
    }

    for (unsigned long i = 0; i < text.length(); i += 2)
        table[(int)text[i]][(int)text[i + 1]]++;


    map<int, int> bigrams;

    for (int i = 0; i < TABLE_SIZE; i++)
        for (int j = 0; j < TABLE_SIZE; j++)
        {   
            if (table[i][j])
            {
                bigrams.insert(pair<int, int> (table[i][j], ALPHABET_SIZE*i + j));
            }
        }

    wofstream ofile(opath);
    map<int, int>::iterator  cur = bigrams.end();

    for (int i = 0; i < 5; i++)
    {
        ofile << (--cur)->second << endl;
    }

    ofile.close();
}


void encrypt(wstring& d_text, int* k, string opath)
{
    wstring alf;
    for ( int i = (int)(L'а'); i <= (int)(L'я'); i++)
    {
        alf.push_back((wchar_t)i);
    }
    alf.erase(alf.find(L'ъ'), 1);

    vector<int> x;
    int tmp = 0;
    for (size_t i = 0; i < d_text.length(); i+=2)
    {
        tmp = (alf.find(d_text[i])*ALPHABET_SIZE + alf.find(d_text[i+1])); 
        x.push_back(tmp);
    }

    wstring str = L"";

    for (size_t i = 0; i < x.size(); i++)
    {
        x[i] = (x[i]*k[0] + k[1]) % BIGRAM_SIZE;
    }

    for (size_t i = 0; i < x.size(); i++)
    {
        str.push_back(alf[x[i] / ALPHABET_SIZE]);
        str.push_back(alf[x[i] % ALPHABET_SIZE]);
    }


    wofstream ofile(opath);
    ofile << str;
    ofile.close();
}


int decrypt(wstring& c_text, int* k, string opath)
{
    wstring alf;
    for ( int i = (int)(L'а'); i <= (int)(L'я'); i++)
    {
        alf.push_back((wchar_t)i);
    }

    alf.erase(alf.find(L'ъ'), 1);


    int a = inverse_element(k[0], BIGRAM_SIZE);
    if (a == -1)
    {
        return -1;
    }
    int b = k[1];
    vector<int> x;
    int tmp = 0;
    for(size_t i = 0; i < c_text.length(); i+=2)
    {
        tmp =  (alf.find(c_text[i])*ALPHABET_SIZE + alf.find(c_text[i+1])); //(a * (((int)c_text[i] - (int)L'а')*ALPHABET_SIZE + (int)c_text[i+1] - (int)L'а' - b)) % BIGRAM_SIZE;
        tmp = (a * (tmp - b) + BIGRAM_SIZE*BIGRAM_SIZE) % BIGRAM_SIZE;
        x.push_back(tmp);
    }

    for (size_t i = 0; i < x.size(); i++)
    {
        if (x[i] < 0) x[i] += BIGRAM_SIZE;
    }

    wstring str;

    for (size_t i = 0; i < x.size(); i++)
    {
        str.push_back(alf[x[i] / ALPHABET_SIZE]);
        str.push_back(alf[x[i] % ALPHABET_SIZE]);
    }

    wofstream ofile(opath);
    ofile << str;
    ofile.close();
    return 0;
}


void foo(string file_t, string file_c, string name)
{
    wifstream ifile(file_t);
    locale::global(locale(""));
    ifile.imbue(locale());
    wstring str = L"";

    int x[5] = {};
    int y[5] = {};

    for (int i = 0; i < 5; i++)
    {
        ifile >> str;
        x[i] = stoi(str);
    }

    ifile.close();
    ifile.open(file_c);

    for (int i = 0; i < 5; i++)
    {
        ifile >> str;
        y[i] = stoi(str);
    }

    ifile.close();

    str = L"";
    ReadFile(name, str);

    int X[2] = {};
    int Y[2] = {};
    multimap<int, int> K;
    int key[2] = {};
    for(int i = 0; i < 5; i++)
    {
        for (int j = i; j < 5; j++)
        {
            for(int k = 0; k < 5; k++)
            {
                for(int l = k; l < 5; l++)
                {
                    X[0] = x[i];
                    X[1] = x[j];

                    Y[0] = y[k];
                    Y[1] = y[l];

                    get_key(X, Y, K);
                    for(pair<int, int> element : K)
                    {
                        key[0] = (element.first + BIGRAM_SIZE) % BIGRAM_SIZE;
                        key[1] = (element.second + BIGRAM_SIZE) % BIGRAM_SIZE;
                        decrypt(str, key, "./decrypt/" + name + "_" + to_string(key[0]) + "_" + to_string(key[1]));                        
                    }
                    K.clear();
                }
            }
        }
    }

}


void FrequencyBigram(wstring& text, map<wstring, double>& bigrams)
{
    double** table = new(nothrow) double* [TABLE_SIZE];

    for (int i = 0; i < TABLE_SIZE; i++)
    {
        table[i] = new(nothrow) double[TABLE_SIZE] {};
    }

    if (text.length() % 2) 
    {
        text = text.substr(0, text.length() - 1);
    }

    for (unsigned long i = 0; i < text.length(); i += 2)
        table[(int)text[i]][(int)text[i + 1]]++;

    wstring tmp;
    double quantity_bigram = text.length() / 2;


    for (int i = 0; i < TABLE_SIZE; i++)
    {
        for (int j = 0; j < TABLE_SIZE; j++)
        {   
            if (table[i][j])
            {
                tmp += (wchar_t)(i + (int)L'а');
                tmp += (wchar_t)(j + (int)L'а');
                bigrams.insert(pair<wstring, double> (tmp, table[i][j] / quantity_bigram));
            }
            tmp = L"";
        }
    }

}



void weed_out(string path)
{
    DIR *dir = opendir(path.c_str());
    wstring text;
    map <wchar_t, double> alf;
    string s = path;
    struct dirent *ent;
    while((ent = readdir(dir)) != NULL)
    {
        if (ent->d_name[0] == '.' ) continue;
        s = path + ent->d_name;
        ReadFile(s, text);
        StringFormatting(text);
        FrequencyChar(text, alf);
        text = L"";
        if (H(alf) > 4.5)
        {
            remove(s.c_str());
        }
        alf.clear();
    }

}

