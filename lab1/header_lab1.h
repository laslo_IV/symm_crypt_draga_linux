#pragma once
#include <string>
#include <fstream>
#include <cstdio>
#include <map>
#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;
const int TABLE_SIZE = 33;

//видалення зайвих символі
void TextFormatting(string pathToText);

void StringFormatting(string& text);

//зчитування файлу в змінну (по замовчуваню з пробілами)
void ReadFile(string ipath, string& text, bool with_space = 1);

//знайти частоти букв в тексті
void FrequencyChar(string& text, string opath);

//знайти чатоти біграм в файлі (з перетинами)
void FrequencyBigram_v1(string& text, string opath);

//знайти чатоти біграм в файлі (без перетинів)
void FrequencyBigram_v2(string& text, string opath);
 
//обрахувати ентропію
double H(string ipath);





//записати частоти n-грам в файл opath (по замовчуваню з перетинами)
//void FrequencyNgram(unsigned int N, string& text, string opath, bool with_intersection = 1);
//int Symbol(unsigned char);
//double H_1(string);
//double H_2(string);
//void FrequencyChar_f(string, string);
//void FrequencyBigram_v1_f(string, string);
//void FrequencyBigram_v2_f(string, string);












